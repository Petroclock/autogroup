# -*- coding: utf-8 -*-
db = DAL('sqlite://storage.db')
db.define_table('ticket', Field('Created', 'date'),
                Field('Time', 'time'),
                Field('Explorer', 'string'),
                Field('People', 'integer'),
                Field('Location', 'string'),
                Field('Leader', 'string'),
                Field('Transport', 'string'),
                Field('Discount_25', 'integer'),
                Field('Discount_50', 'integer'),
                Field('Discount_100', 'integer'),
                Field('Amount_services', 'string'),
                Field('Summary', 'integer'),
                Field('Status', 'boolean'),
                Field('Pay', 'boolean'),
                Field('Number_ticket', 'string'))

db.define_table('service',
                Field('name', 'string'),
                Field('sum_summer', 'integer'),
                Field('sum_summer_weekand', 'integer'),
                Field('sum_winter', 'integer'),
                Field('status', 'boolean')
                )

db.define_table('list_service',
                Field('ticket_id', 'reference ticket'),
                Field('service', 'string'),
                Field('price', 'integer'),
                Field('created', 'date'),
                Field('people', 'integer')
                )
