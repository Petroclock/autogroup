# -*- coding: utf-8 -*-

import json
import datetime
import socket
import calendar


def service_count(service, date):
    Kol_vo = 0
    Dict = db(db.list_service.created == date).select(db.list_service.ALL)
    for item in Dict:
        if (item['service'].find(service)) >= 0:
            Kol_vo = Kol_vo + item['people']
    return Kol_vo


# Список услуг для страницы вывода услуг
def services():
    if not request.post_vars:
        return
    response = request.post_vars
    date = response['Date']
    date = datetime.datetime.strptime(date, "%Y-%m-%d")
    date = date.date()
    day = date.day
    month = date.month
    year = date.year
    week = calendar.weekday(year, month, day)
    service = []
    info = {'name': '', 'price': 0, 'count': 0}
    if (month >= 5) and (month < 10):
        if (week == 5) or (week == 6):
            period = 'sum_summer_weekand'
        else:
            period = 'sum_summer'
    else:
        period = 'sum_winter'
    services = db(db.service.status == 'true').select()
    for item in services:
        info['name'] = item.name
        info['price'] = item[period]
        info['count'] = service_count(item.name, response[u'Date'])
        service.append(info)
        info = {'name': '', 'price': 0, 'count': 0}
    return dict(service=service)


# TODO передавать объект сразу на запись не помещая его в словарь

def new_group():
    if not request.post_vars:
        return
    else:
        data = request.post_vars
    my_time = datetime.datetime.today().timetz()
    group = {'Created': data.Created,
             'Time': my_time,
             'People': data.People,
             'Leader': data.Leader,
             'Amount_services': data.Amount_services,
             'Pay': data.Pay,
             'Summary': data.Summary
             }
    list_service = json.loads(request.post_vars.uslugi)
    if request.post_vars.id == 0 or request.post_vars.id == '0':
        id = db.ticket.insert(data)
        db(db.list_service.ticket_id == id).delete()
        for item in list_service:
            db.list_service.insert(ticket_id=id,
                                   service=item['name'],
                                   price=item['price'],
                                   created=data.Date,
                                   people=data.Kol_vo
                                   )
    elif int(request.post_vars.id) > 0:
        id = int(request.post_vars.id)
        db(db.ticket.id == id).update(data)
        db(db.list_service.ticket_id == id).delete()
        for item in list_service:
            db.list_service.insert(ticket_id=id,
                                   service=item['name'],
                                   price=item['price'],
                                   created=data.Date,
                                   people=data.Kol_vo
                                   )

    if not request.post_vars.Pay:
        return dict(id=id)
    return


def search_leader():
    response = request.post_vars
    data = db((db.ticket.Leader == response['Leader']) & (db.ticket.Pay is False))
    data.select(db.ticket.ALL, db.list_service.ALL, left=db.list_service.on(db.ticket.id == db.list_service.ticket_id))
    return dict(Data=data)


def search_id():
    response = request.post_vars
    data = db((db.ticket.id == response['id']) & (db.ticket.Pay is False))
    data.select(db.ticket.ALL, db.list_service.ALL, left=db.list_service.on(db.ticket.id == db.list_service.ticket_id))
    return dict(Data=data)


def payment():
    Data = request.post_vars
    ID = Data['id']
    Point = Data['Point']
    my_time = datetime.datetime.today().timetz()
    my_time = my_time.strftime('%H:%M:%S')
    db.ticket.update_or_insert(db.ticket.id == ID, oplata='Оплачено', Point=Point, Time=my_time)
    return True


def check():
    return True


# Кол-во человек на экскурсию в пещеру
def peoples_count():
    response = {'wait': 0, 'no_wait': 0}
    now_date = datetime.date.today()
    people = db(db.ticket.Created == now_date).select(db.ticket.People, db.ticket.Status)
    for item in people:
        if item['status']:
            response['wait'] = response['wait'] + item.People
        elif not item['status']:
            response['no_wait'] = response['no_wait'] + item.People
    return response


def list_tickets():
    now_date = datetime.date.today()
    response = db((db.ticket.Created == now_date) & (db.ticket.Pay is True)).select()
    req = []
    res = {}
    for i in response:
        res['Time'] = i['Time']
        res['People'] = i['People']
        res['Summary'] = i['Summary']
        req.append(res)
        res = {}
    return dict(req=req)
