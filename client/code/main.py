# -*- coding: utf-8 -*-
import os
import Func
import os.path
from kivy.config import ConfigParser
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemeManager
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.properties import ObjectProperty
from s_module.widgets import Btn, InfoTextField
from s_module.widgets import GroupCard, FlatBtn
from s_module.check import Statistics as Stats
from s_module.group import Group, GroupList
from s_module.modal import Msg


class CashApp(App):
    c = ConfigParser()
    theme_cls = ThemeManager()
    icon = 'kugar.png'

    # Панель настроек
    def __init__(self, **kwargs):
        super(CashApp, self).__init__(**kwargs)
        self.link = ''
        # Заголовки
        self.Titles = {'title1': '', 'title2': '', 'title3': ''}
        self.is_stop = False

    def build_settings(self, settings):
        self_config = os.path.abspath('cash.ini')
        self.c.read(self_config)
        jsondata = '[{"type":"title", "title":"Настройки"},' \
                   '{"type":"string","title":"Ссылка", "section": "Server", "key": "link"},' \
                   '{"type":"string","title":"Заголовок 1", "section": "Title", "key": "Title1"},' \
                   '{"type":"string","title":"Заголовок 2", "section": "Title", "key": "Title2"},' \
                   '{"type":"string","title":"Заголовок 3", "section": "Title", "key": "Title3"}'
        settings.add_json_panel('Settings',
                                self.c, data=jsondata)

    # Настройки
    def build_config(self, config):
        config.setdefaults('Server',
                           {'link': self.link,
                            'Ttile1': self.Titles['title1'],
                            'Title2': self.Titles['title2'],
                            'Title3': self.Titles['title3'], })

    # Построение приложения
    def build(self):
        config = self.config
        self.Titles['title1'] = config.get('Title', 'Title1')
        self.Titles['title2'] = config.get('Title', 'Title2')
        self.Titles['title3'] = config.get('Title', 'Title3')
        self.link = config.get('Server', 'link')
        return Builder.load_file('kv/main.kv')

    # Сохраняет настройки
    def on_config_change(self, config, section, key, value):
        self.link = config.get('Server', 'link')
        self.Titles['title1'] = config.get('Title', 'Title1')
        self.Titles['title2'] = config.get('Title', 'Title2')
        self.Titles['title3'] = config.get('Title', 'Title3')

    def on_stop(self):
        print 'on_stop...'
        self.is_stop = True
        os.abort()


# Вкладка регистрации группы
class Recording(BoxLayout):
    def __init__(self, **kwargs):
        super(Recording, self).__init__(**kwargs)
        self.App = App.get_running_app()
        self.orientation = 'vertical'
        # Объект регистрации группы
        self.group = Group(self)


# Вкладка Посетители
class People(GridLayout):
    def __init__(self, **kwargs):
        super(People, self).__init__(**kwargs)
        self.App = App.get_running_app()
        self.stats = Stats(self.App)

    # Вывод статистики за сегодня
    def press_statis(self):
        self.stats.open_modal()


if __name__ == '__main__':
    CashApp().run()