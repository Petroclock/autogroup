# -*- coding: utf-8 -*-
import unittest
import datetime
import json
from s_module.group import Group
from s_module.group import GroupList
from s_module.group import DispenserGroup
from s_module.check import Statistics
from s_module.request import Request
from s_module.turnstile import Turnstile


class TestWidget:
    def __init__(self, name, text):
        self.text = text
        self.name = name
        self.pointNum = 2


class SendGroup:
    def __init__(self):
        self.data = {'Date': datetime.datetime.today().date(),
                     'Summa_Uslug': self.prices['price_service'],
                     'shtr': self.bar_code,
                     'Pay': self.pay,
                     'Kol_vo': self.group_info['Kol_vo'],
                     'Yers8': self.group_info['Yers8'],
                     'Yers16': self.group_info['Yers16'],
                     'L_25': self.group_info['Lgoti_25'],
                     'L_50': self.group_info['Lgoti_50'] + self.group_info['Lgita'],
                     'L_100': self.group_info['Lgoti_100'],
                     'Where': self.group_info['Where'],
                     'Rucovoditel': self.group_info['Rucovoditel'],
                     'Vehicle': self.group_info['Vehicle'],
                     'Itog': self.prices['itog_summa'],
                     'Point': self.point,
                     'NumberOfTicket': self.group_info['NumberOfTicket'],
                     'uslugi': json.dumps(self.service),
                     'id': 1}


class ResponseGroup:
    def __init__(self):
        self.response_group = {'Data': [{'id': 1,
                                         'Created': datetime.datetime.today(),
                                         'Kol_vo': self.group_info['Kol_vo'],
                                         'Otkuda': self.group_info['Where'],
                                         'Rucovoditel': self.group_info['Rucovoditel'],
                                         'Sredstvo_peredvijenia': self.group_info['Vehicle'],
                                         'Lgoti_25': self.group_info['Lgoti_25'],
                                         'Lgoti_50': self.group_info['Lgoti_50'] +
                                                     self.group_info['Lgita'],
                                         'Lgoti_100': self.group_info['Lgoti_100'],
                                         'summa_uslug': self.prices['price_service'],
                                         'Itog': self.prices['itog_summa'],
                                         'Shtrihcode': self.bar_code,
                                         'Deti_do_8': self.group_info['Yers8'],
                                         'Deti_do_16': self.group_info['Yers16'],
                                         'NumberOfTicket': self.group_info['NumberOfTicket']}],
                               'list': [[{'service': '1', 'price': '100'},
                                         {'service': '2', 'price': '200'},
                                         {'service': '3', 'price': '300'}]]}
        self.response_fields = ['Created', 'Kol_vo', 'Otkuda', 'Rucovoditel', 'Sredstvo_peredvijenia',
                                'Lgoti_25', 'Lgoti_50', 'Lgoti_100', 'summa_uslug', 'Itog', 'id',
                                'Shtrihcode', 'Deti_do_8', 'Deti_do_16', 'NumberOfTicket']


# Тестовые данные
class TestData:
    def __init__(self, pay='Оплачено'):
        self.link = 'http://127.0.0.1:8000/'
        self.Titles = {'title1': 't1', 'title2': 't2', 'title3': 't3'}
        self.translater = False
        self.bar_code = 100000000000
        self.pay = pay
        self.point = '2'
        self.print_width = 500
        self.turnstile = TestWidget('turnstile', 'pointNum')

        self.fields = {'Date': datetime.datetime.today().date(),
                       'Where': 'Str',
                       'Rucovoditel': 'Rus',
                       'Vehicle': 'Vaz',
                       'NumberOfTicket': 'Number',
                       'Kol_vo': 20,
                       'Lgoti_25': 1,
                       'Lgoti_50': 2,
                       'Lgoti_100': 3,
                       'Lgita': 1,
                       'Yers8': 2,
                       'Yers16': 3}

        self.service = [{'name': '1', 'price': 100},
                        {'name': '2', 'price': 200},
                        {'name': '3', 'price': 300}]

        self.prices = {'price_service': 600,
                       'itog_summa': 7050}


# Группа
class TestGroup(unittest.TestCase):
    def setUp(self):
        self.app = TestData()
        self.group = Group(self.app)
        self.widgets = {}
        for key, val in self.app.fields.items():
            self.widgets[key] = TestWidget(key, val)
        self.price_label = TestWidget('price_label', '0')
        return

    def test_record(self):
        # Вводим данные
        self.group.data = self.widgets['Date']
        self.group.change_service = self.app.service
        self.group.barcode = self.app.bar_code
        for key, val in self.widgets.items():
            self.group.get_data(val, self.widgets, self.price_label)
        self.check_group()
        # Создать чек
        self.group.load_check()
        self.check_check()
        # Распечатать чек
        self.group.print_check()
        # Отправить данные на сервер
        self.group.request_group(self.app.pay)
        self.assertTrue(self.group.request_group(self.app.pay))
        # Очистить группу
        self.group.reload()
        self.assertEqual(self.group, Group(self.app))
        return

    def check_group(self):
        self.assertEqual(self.group.price_service, self.app.prices['price_service'])
        self.assertEqual(self.group.summary, self.app.prices['itog_summa'])
        return

    def check_check(self):
        check = self.group.payment_check.check
        group_info = {'people': 20,
                      'l_25': 1,
                      'l_50': 6,
                      'l_100': 5,
                      'barcode': self.app.bar_code,
                      'total': 7050}
        self.assertEqual(len(check.title), 5)
        self.assertEqual(len(check.content), 22)
        self.assertEqual(len(check.summary), 2)
        self.assertEqual(check.barcode, self.app.bar_code)
        for key, val in group_info.items():
            self.assertEqual(check.group_info[key], val)
        self.assertEqual(check.services, self.app.service)
        return


# Список групп
@unittest.SkipTest
class TestGroupList(unittest.TestCase):
    def setUp(self):
        self.group_list = GroupList()
        self.data = TestData(id=1)
        self.groups = self.data.response_group
        # Чек оплаты группы
        self.payment_check = Payment()
        self.title = {'Title1': 'T1', 'Title2': 'T2', 'Title3': 'T3', 'translater': '5'}
        self.request = Request()
        self.request.link = 'http://127.0.0.1:8000/'
        return

    @unittest.SkipTest
    def test_search(self):
        group = self.group_list.create_list(self.groups)

        # Блок проверки получения группы
        self.assertIsInstance(group, Group)
        self.assertEqual(group.date, self.data.response_group['Data'][0]['Created'])
        self.assertEqual(group.translater, self.data.translater)
        self.assertEqual(group.barcode, self.data.bar_code)
        self.assertEqual(group.price_service, self.data.response_group['Data'][0]['summa_uslug'])
        self.assertEqual(group.summary, self.data.response_group['Data'][0]['Itog'])
        self.assertEqual(group.id, self.data.response_group['Data'][0]['id'])
        self.assertEqual(group.change_service, self.data.response_service)
        for key, val in self.data.test_group_info.items():
            if key in self.data.ignore_fields:
                continue
            self.assertEqual(group.info[key], val)

        # Проверка валидации переменных
        # Проверка на валидацию данных
        validate_data = group.validate_data()
        self.assertTrue(validate_data)
        for key, val in self.data.test_group_info.items():
            if key in self.data.ignore_fields:
                continue
            self.assertEqual(group.info[key], val)

        # Проверка правильности подсчёта суммы
        group.count_summary()
        self.assertEqual(group.price_service, self.data.prices['price_service'])
        self.assertEqual(group.summary, self.data.prices['itog_summa'])

        # Готовим чек для распечатки
        self.payment_check.ready(self.title, group)

        # Печатаем чек
        printer = Printer(self.payment_check.check, 500)
        bar_code = printer.print_bar_code()
        self.assertTrue(bar_code)

        # Отправка группы на запись
        data = group.load_data('Оплачено', '2')
        self.assertEqual(data, self.data.send_group)
        request = self.request.write_group(data, 'Оплачено')
        self.assertIsNot(request, False)
        return


# Распределитель групп
@unittest.SkipTest
class TestDispenserGroup(unittest.TestCase):
    def setUp(self):
        self.widget = GridLayout(cols=3)
        self.dispenser = DispenserGroup(self.widget, 'http://127.0.0.1:8000/')
        self.all = [{'Rucovoditel': 'Rus',
                     'Kol_vo': 15,
                     'NumberOfTicket': 'Number',
                     'Otkuda': 'Str',
                     'barCode': 20,
                     'ticket': 3,
                     'text': u'Руководитель: Rus\nКол-во посетителей: 15\n№ квитанции: Number\nОткуда: Str'
                     }]
        self.wait = [{'Rucovoditel': 'Rus',
                      'Kol_vo': 10,
                      'NumberOfTicket': 'Numb',
                      'Otkuda': 'Str',
                      'barCode': 30,
                      'ticket': 2,
                      'text': u'Руководитель: Rus\nКол-во посетителей: 10\n№ квитанции: Numb\nОткуда: Str'
                      }]
        self.tour = [{'Rucovoditel': 'Rus',
                      'NumberOfTicket': 'NoTic',
                      'Otkuda': 'Str',
                      'barCode': 10,
                      'ticket': 1,
                      'Kol_vo': 5,
                      'text': u'Руководитель: Rus\nКол-во посетителей: 5\n№ квитанции: NoTic\nОткуда: Str'
                      }]
        self.test_data = {'kpp': [{'Rucovoditel': 'Rus',
                                   'Kol_vo': 10,
                                   'NumberOfTicket': 'Numb',
                                   'Otkuda': 'Str',
                                   'Shtrihcode': 30,
                                   'id': 2
                                   }],
                          'wait': [
                              {'ticket': {'Rucovoditel': 'Rus',
                                          'NumberOfTicket': 'NoTic',
                                          'Otkuda': 'Str',
                                          'Shtrihcode': 10,
                                          'id': 1
                                          },
                               'ticket_to_cave': {'kol_vo': 5}
                               }
                          ],
                          'data': [{'Rucovoditel': 'Rus',
                                    'Kol_vo': 15,
                                    'NumberOfTicket': 'Number',
                                    'Otkuda': 'Str',
                                    'Shtrihcode': 20,
                                    'id': 3
                                    }]
                          }
        return

    @unittest.SkipTest
    def test_create_lists(self):
        self.dispenser.create_lists('', self.test_data)
        self.assertEqual(self.dispenser.all_group, self.all)
        self.assertEqual(self.dispenser.wait_group, self.wait)
        self.assertEqual(self.dispenser.tour_group, self.tour)
        return

    @unittest.SkipTest
    def test_cut_group(self):
        self.dispenser.create_lists('', self.test_data)
        adapter = u'Руководитель: Rus\nКол-во посетителей: 10\n№ квитанции: Numb\nОткуда: Str'
        self.dispenser.cut_group(adapter)
        self.assertEqual(len(self.dispenser.wait_group), 0)
        self.assertEqual(len(self.dispenser.tour_group), 2)
        return

    @unittest.SkipTest
    def test_select_group_all(self):
        self.dispenser.create_lists('', self.test_data)
        self.dispenser.select_group(TextInput(text='10'), 2)
        self.assertEqual(len(self.dispenser.wait_group), 0)
        self.assertEqual(len(self.dispenser.tour_group), 2)
        return

    @unittest.SkipTest
    def test_select_unselect_group_half(self):
        self.dispenser.create_lists('', self.test_data)
        self.dispenser.select_group(TextInput(text='5'), 2)
        self.assertEqual(len(self.dispenser.wait_group), 1)
        self.assertEqual(len(self.dispenser.tour_group), 2)
        self.assertTrue(self.dispenser.wait_group[0]['Kol_vo'] == 5)
        self.assertTrue(self.dispenser.tour_group[0]['Kol_vo'] == 5)
        self.assertTrue(self.dispenser.wait_group[0][
                            'text'] == u'Руководитель: Rus\nКол-во посетителей: 5\n№ квитанции: Numb\nОткуда: Str')
        self.assertTrue(self.dispenser.tour_group[1][
                            'text'] == u'Руководитель: Rus\nКол-во посетителей: 5\n№ квитанции: Numb\nОткуда: Str')
        self.dispenser.unselect_group(u'Руководитель: Rus\nКол-во посетителей: 5\n№ квитанции: Numb\nОткуда: Str')
        self.assertEqual(len(self.dispenser.wait_group), 1)
        self.assertEqual(len(self.dispenser.tour_group), 1)
        self.assertTrue(self.dispenser.wait_group[0][
                            'text'] == u'Руководитель: Rus\nКол-во посетителей: 10\n№ квитанции: Numb\nОткуда: Str')
        self.assertTrue(self.dispenser.tour_group[0][
                            'text'] == u'Руководитель: Rus\nКол-во посетителей: 5\n№ квитанции: NoTic\nОткуда: Str')
        return

    @unittest.SkipTest
    def test_search_add_group(self):
        self.dispenser.create_lists('', self.test_data)
        self.dispenser.search_of_barcode('20')
        self.assertEqual(self.dispenser.search_data, {'field': 'barCode', 'data': '20'})
        self.dispenser.search_of_number('Number')
        self.assertEqual(self.dispenser.search_data, {'field': 'NumberOfTicket', 'data': 'Number'})
        self.dispenser.add_group()
        self.assertEqual(len(self.dispenser.wait_group), 2)
        self.assertEqual(len(self.dispenser.all_group), 0)
        return


# Чек статистики
@unittest.SkipTest
class TestStatistic(unittest.TestCase):
    def setUp(self):
        self.statistic = Statistics()
        self.content = [{
            'NumberOfTicket': '1',
            'Time': '14:24',
            'Kol_vo': '20',
            'Itog': '1500',
            'Point': '2'
        }]
        self.point = u'2'
        self.main = {'Title1': 'T1', 'Title2': 'T2', 'Title3': 'T3', 'translater': '5'}
        self.test_text = u'Итого: \n' + u'-Билетов: 1' + u'\n-Сумма: 1500'
        return

    @unittest.SkipTest
    def test_ready(self):
        text = self.statistic.ready(self.content, self.main, self.point)
        self.assertEqual(text, self.test_text)
        self.assertEqual(len(self.statistic.text_dict['title']), 4)
        self.assertEqual(len(self.statistic.text_dict['content']), 1)
        self.assertEqual(len(self.statistic.text_dict['itog']), 3)
        return


# Объект запросов
@unittest.SkipTest
class TestRequest(unittest.TestCase):
    def setUp(self):
        self.request = Request()
        self.request.link = 'http://127.0.0.1:8000/'
        self.date = datetime.datetime.today()
        self.data = {}
        return

    # TODO переписать контроллеры на сервере для проверки правильности данных
    @unittest.SkipTest
    def requests(self):
        bar_code = self.request.get_barcode()
        self.assertIsInstance(bar_code, int)
        service = self.request.get_services(self.date)
        self.assertIsInstance(service, dict)
        self.assertEqual(self.request.write_group(self.data, 'Оплачено'), True)
        search_group_id = self.request.search_from_id(1)
        self.assertIsInstance(search_group_id, dict)
        search_group_rucovoditel = self.request.search_from_rucovoditel('Rus')
        self.assertIsInstance(search_group_rucovoditel, dict)
        self.assertEqual(self.request.pay_bron(1, 2), True)
        self.assertEqual(self.request.send_group(), True)
        self.assertEqual(self.request.add_wait_group(1, 20), True)
        self.assertEqual(self.request.add_kpp_group(1), True)
        list_of_group = self.request.list_of_group()
        self.assertIsInstance(list_of_group, dict)
        return


# Турникет
@unittest.SkipTest
class TestTurnstile(unittest.TestCase):
    @unittest.SkipTest
    def test_turnstile(self):
        self.turnstile = Turnstile()
        self.assertIsNot(self.turnstile, None)
        self.turnstile.pointNum = u'2'
        self.assertEqual(self.turnstile.open_door(), True)
        self.assertEqual(self.turnstile.close_door(), True)
        self.assertEqual(self.turnstile.update_status_label(), True)
        return


if __name__ == '__main__':
    unittest.main()
