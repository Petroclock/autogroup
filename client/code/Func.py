# -*- coding: utf-8 -*-
import calendar
import datetime
from kivy.adapters.listadapter import ListAdapter
from kivy.uix.listview import ListView


# Функция проверки строки
def val_to_int(some_text):
    try:
        text = int(some_text)
    except ValueError:
        text = 0
    return text


# Функция очистки текстовых полей в Виджете
def clear_text(widget):
    for children in widget.children:
        if children.type != 'button' and children.type != 'label' and children.type != 'textDate':
            children.text = ''


# Функция построения списка
def create_list(widget,
                adapter_list,
                data_list,
                converter,
                cls_list,
                selection_mode_list,
                allow_empty_selection_list):
    if adapter_list is not None:
        widget.add_widget(ListView(adapter=adapter_list))
        return adapter_list
    new_adapter = ListAdapter(data=data_list,
                              args_converter=converter,
                              cls=cls_list,
                              selection_mode=selection_mode_list,
                              allow_empty_selection=allow_empty_selection_list)
    new_list_view = ListView(adapter=new_adapter)
    widget.add_widget(new_list_view)
    return new_adapter


# Создать словарь из имени и объектов дочерних текстовых полей нужного виджета
def text_dict(widget):
    if isinstance(widget, dict):
        return widget
    t_dict = {}
    for item in widget.children:
        if item.type == 'text' or item.type == 'spinner' or item.type == 'textDate':
            t_dict[item.name] = item
        if item.type == 'grid':
            for key in item.children:
                if key.type == 'text' or key.type == 'spinner' or item.type == 'textDate':
                    t_dict[key.name] = key
    return t_dict


# Проверка на скидку
def place_discont(text, date):
    is_check = False
    try:
        text = text.decode('utf8')
    except:
        text = text
    if text == u'Бурзянский район':
        day = date.day
        month = date.month
        year = date.year
        week = calendar.weekday(year, month, day)
        cancel = False
        i = 7
        while i < 8:
            if month == i:
                if (week == 5) or (week == 6):
                    cancel = True
            i += 1
        if not cancel:
            is_check = True
    return is_check


# Преобразование словаря в строку по ключам
def dict_to_str(request):
    listing = ''
    for item in request:
        try:
            listing += str(item) + ','
        except:
            listing += item.encode('utf8') + ','
    listing = listing[0:-1]
    return listing


# Вырезать фразу между двух слов
def cut_between(text, first, last=''):
    leng = len(text)
    leng_first = len(first)
    leng_last = len(last)
    text = text[0 + leng_first: leng - leng_last]
    return text


# Преобразовать в дату
def create_date(date_string):
    if isinstance(date_string, str):
        try:
            date = datetime.datetime.strptime(date_string, "%d.%m.%Y")
        except ValueError:
            date = datetime.datetime.strptime(date_string, "%Y-%m-%d")
        return date.date()
    else:
        return date_string


# Преобразование кодировки
def to_utf8(dict):
    for item in dict:
        try:
            dict[item].text = dict[item].text.encode('utf8')
        except:
            dict[item].text = dict[item].text
    return dict


#
def new_text(dict):
    dict['text'] = u'Руководитель: ' + dict['Rucovoditel'] + '\n' + u'Кол-во посетителей: ' + unicode(
        dict['Kol_vo']) + '\n' + u'№ квитанции: ' + unicode(dict['NumberOfTicket']) + '\n' + u'Откуда: ' + dict[
                       'Otkuda']
    return dict


# Искать дукликат по ид в списке
def search_duplicate(list, id):
    duplicate = False
    duplicate_index = 0
    for item in list:
        if item['ticket'] == id:
            duplicate = True
            duplicate_index = list.index(item)
    return duplicate, duplicate_index


# Удалить из массива словарь в ключе field которого есть указанное число или больше
def delete_item(list, count, field):
    index_eq = False
    index_big = False

    # Шаг 1 Пытаемся найти подходящий элемент
    for item in list:
        if item[field] > count:
            index_big = item
        if item[field] == count:
            index_eq = item
    # Шаг 2 если мы нашли подходящий элемент то удаляем его
    if index_eq:
        list.remove(index_eq)
        return list
    if index_big:
        list.remove(index_big)
        return list


# Считать кол-во по выбранному полю
def count_to_field(list, field):
    count = 0
    for item in list:
        count += int(item[field])
    return count


# Найти словарь в списке с заданым значением ключа field
def serch_dict(list, key, value):
    for item in list:
        if item[key] == value:
            return item
    return False


#
def validate_search_data(search):
    if len(search) == 0:
        return '', ''
    elif search.isdigit():
        s_r = ''
        s_id = search
    elif search.isalpha():
        s_r = search
        s_id = ''
    else:
        s_r = ''
        s_id = ''
    return s_r, s_id
