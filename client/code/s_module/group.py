# -*- coding: utf-8 -*-
from kivy.app import App
import datetime
import json
from kivy.uix.widget import Widget
import Func
from info import Info
from service import Service
from s_module.request import Request
from s_module.check import Payment
from s_module.widgets import InfoTextField
from kivymd.textfields import MDTextField
from widgets import Label, Btn
from kivy.uix.gridlayout import GridLayout
from modal import Msg
from kivy.properties import ObjectProperty
from s_module.date_picker import MDDatePicker
from widgets import GroupCard
from collections import OrderedDict
from kivy.uix.anchorlayout import AnchorLayout


class Fields:
    def __init__(self):
        self.list_service = ['ticket_id',
                             'service',
                             'price',
                             'created',
                             'people']

        self.num_fields = ['People', 'Preferential', 'Child_8', 'Child_16']
        self.string_field = ['Leader']
        self.summary_field = ['Created', 'People', 'Child_8', 'Child_16', 'Preferential']

        self.discount_full = ['Child_8']
        self.discount_half = ['Child_16', 'Preferential']

        self.info_fields = OrderedDict()
        self.info_fields['Created'] = 'Дата'
        self.info_fields['People'] = 'Общее кол-во человек'
        self.info_fields['Child_8'] = 'Дети до 8 лет'
        self.info_fields['Child_16'] = 'Дети до 16 лет'
        self.info_fields['Preferential'] = 'Инвалиды, пенсионеры, ветераны'
        self.info_fields['Leader'] = 'Фамилия Имя'
        self.info_text_field = ['Created']
        self.info_num_field = ['People', 'Child_8', 'Child_16', 'Preferential']
        self.info_str_field = ['Leader']


class Group:
    def __init__(self, parent):
        self.app = App.get_running_app()
        self.parent = parent
        self.fields = Fields()
        self.id = 0
        self.date = datetime.date.today()
        self.change_service = []
        self.price_service = 0
        self.summary = 0
        self.price_widget = Label(text="0")
        self.info = {}
        self.payment_check = Payment(self.app)
        self.request = Request()
        self.request.link = self.app.link
        self.info_widget = Info(self)
        self.service_widget = Service(self)

        # Поле поиска
        self.search_field = ObjectProperty()
        # Виджет группы
        self.group_widget = GroupCard()
        self.build_widgets()

    def build_widgets(self):
        panel = GridLayout(cols=3)
        self.parent.add_widget(panel)
        panel.add_widget(self.service_widget)
        panel.add_widget(self.info_widget)
        self.info_widget.build()
        pay = Btn(text="Оплатить")
        pay.fbind('on_press', self.press_pay, self.info, True)
        bron = Btn(text="Бронь")
        bron.fbind('on_press', self.press_pay, self.info, False)
        sum_grid = GridLayout(cols=2)
        sum_grid.add_widget(Label(text="Сумма"))
        sum_grid.add_widget(self.price_widget)
        sum_grid.add_widget(pay)
        sum_grid.add_widget(bron)
        self.info_widget.add_widget(sum_grid)

        self.search_field = InfoTextField('Поиск по ID'.decode('utf8'), "ID группы")
        search = Btn(text="Найти")
        search.fbind('on_press', self.press_search, self.search_field)
        cancel = Btn(text='Отмена')
        save = Btn(text='Сохранить')
        pay_bron = Btn(text='Оплатить')
        search_grid = GridLayout(cols=1, spacing=[0, 20])
        search_panel_1 = AnchorLayout(anchor_x='center', anchor_y='center', padding=[40, 100])
        search_grid.add_widget(self.search_field)
        search_grid.add_widget(search)
        search_grid.add_widget(cancel)
        search_grid.add_widget(save)
        search_grid.add_widget(pay_bron)
        search_panel_1.add_widget(search_grid)
        panel.add_widget(search_panel_1)

    # Поиск брони
    def press_search(self, text_field, btn):
        if len(text_field.text) == 0:
            return
        search = text_field.text
        text_field.text = ''
        s_r, s_id = Func.validate_search_data(search)

        if s_r != '':
            response = self.App.request.search_from_leader(s_r)
        elif s_id != '':
            response = self.App.request.search_from_id(s_id)
        else:
            Msg('Введите данные')
            return
        if not response:
            return
        self.search_group.create_list(response)

    def clear_widget(self):
        for key, val in self.info_widget.items():
            val.text = ''
        self.__init__(self.app)
        for key, val in self.fields.items():
            if isinstance(self.fields[key], Widget):
                self.fields[key].text = ''
            else:
                self.fields[key] = ''

    # Оплата заказа
    def press_pay(self, information, pay):
        if not self.validate_data():
            Msg('Проверьте внесённые данные: Дата, Услуги, Количество человек')
            return
        self.load_check()

        if pay:
            if not self.print_check():
                Msg('Невозможно распечатать чек.')
                return
        result = self.request_group(pay)
        if pay:
            if result:
                Msg('Группа оплачена')
            else:
                Msg('Ошибка подключения. Проверьте подключение и попробуйте снова.')
                return
        else:
            if result:
                Msg('Группа забронированна. ID: ' + result)
            else:
                Msg('Ошибка получения ID')
                return

        Func.clear_text(information)
        self.service_widget.list_of_service.clear_widgets()
        self.reload()
        self.price_widget.text = '0'
        self.date = ObjectProperty()

    def set_previous_date(self, date_obj):
        info = self.parse_info(self.info_widget)
        self.date = date_obj
        info['Created'].text = self.date.strftime('%d.%m.%Y')
        # info['Created'].hint_anim_in.start(info['Created'])
        self.date = Func.create_date(self.date)
        self.service_widget.create_services(self.service_widget.list_of_service, self.date)

    def date_picker(self, instance, value):
        if not instance.focus:
            return
        pd = self.date
        try:
            MDDatePicker(self.set_previous_date,
                         pd.year, pd.month, pd.day).open()
        except AttributeError:
            MDDatePicker(self.set_previous_date).open()

    def parse_info(self, info):
        widgets = {}
        for item in info.children:
            if isinstance(item, MDTextField):
                widgets[item.name] = item
        return widgets

    # Заполнение данных
    def get_data(self, btn, text):
        info = self.info_widget
        price_label = self.price_widget
        widgets = self.parse_info(info)
        if btn.name == 'Created' or len(widgets) == 0:
            return
        # По условию либо заполняем пустой словарь либо перезаписываем сущ. значение
        if len(self.info) == 0:
            for field in self.fields.summary_field:
                self.info[field] = widgets[field].text
            for field in self.fields.string_field:
                self.info[field] = widgets[field].text
        else:
            self.info[btn.name] = widgets[btn.name].text

        # Проверка на группы льгот (для корректного полсчёта итоговой суммы)
        if btn.name in self.fields.discount_full:
            for field in self.fields.discount_full:
                self.info[field] = widgets[field].text
        if btn.name in self.fields.discount_half:
            for field in self.fields.discount_half:
                self.info[field] = widgets[field].text

        # Если изменяется поле влияющее на итоговую сумму то пересчитываем сумму
        for field in self.fields.summary_field:
            if field == btn.name:
                self.count_summary()
                price_label.text = str(self.summary)
                return
        return

    # Проверка переменных
    def validate_variable(self):
        if len(self.info) == 0:
            for item in self.fields.num_fields:
                self.info[item] = 0
            for item in self.fields.string_field:
                self.info[item] = ''
            return
        for item in self.fields.num_fields:
            if isinstance(self.info[item], Widget):
                self.info[item] = Func.val_to_int(self.info[item].text)
            else:
                self.info[item] = Func.val_to_int(self.info[item])
        for item in self.fields.string_field:
            if isinstance(self.info[item], Widget):
                try:
                    self.info[item] = self.info[item].text.encode('utf8')
                except:
                    self.info[item] = self.info[item].text
            elif isinstance(self.info[item], unicode):
                self.info[item] = str(self.info[item].encode('utf8'))
        return

    def validate_services(self):
        for service in self.change_service:
            service['price'] = int(service['price'])
        return

    # Проверка на обязательные данные
    def validate_data(self):
        self.validate_variable()
        required = ['People']
        for item in required:
            if isinstance(self.info[item], int):
                if self.info[item] == 0:
                    # print 'Empty field ', item
                    return False
            elif isinstance(self.info[item], Widget):
                if len(self.info[item].text) == 0 or self.info[item].text == '0':
                    # print 'Empty textfield ', item
                    return False
        if not self.date:
            # print 'Empty date'
            return False
        elif not isinstance(self.date, datetime.datetime):
            self.date = Func.create_date(self.date)
            return True
        elif len(self.change_service) == 0:
            # print 'Empty service'
            return False
        return True

    # Проверка на количество человек
    @staticmethod
    def recount(max_count, l_50, l_100):
        if l_100 >= max_count:
            l_100 = max_count
            l_50 = 0
        if l_50 >= max_count:
            l_100 = 0
            l_50 = max_count
        return max_count, l_50, l_100

    def count_summary(self, *args, **kwargs):
        if not self.validate_data():
            return False
        self.summary = 0

        # Считаем общую сумму услуг
        for service in self.change_service:
            try:
                self.summary += int(service['price'])
            except ValueError:
                self.summary += 0

        self.price_service = self.summary
        self.validate_variable()
        self.summary = self.count_price(self.summary)

    # Посчитать стоимость
    def count_price(self, price):
        """
        Посчитать сумму с учётом всех факторов
        :param price: либо сумма всех услуг, либо сумма отдельной услуги
        :return: итоговая стоимость
        """
        people = int(self.info['People'])
        l_50 = int(self.info['Child_16'] + self.info['Preferential'])
        l_100 = int(self.info['Child_8'])
        people, l_50, l_100 = self.recount(people, l_50, l_100)
        service_summa = int(price)
        people -= l_50 + l_100
        price_no_discont = people * service_summa
        sum__l50 = l_50 * (service_summa / 2)
        itog = price_no_discont + sum__l50
        return itog

    # Обновить объект Группа (сбросить по умолчанию значения свойств)
    def reload(self):
        self.__init__(self.parent)

    # Подготовка данных на отправку
    def load_data(self, pay):
        self.validate_data()
        self.validate_services()
        # listing = Func.dict_to_str(self.change_service)
        listing = json.dumps(self.change_service)
        data = {'Date': self.date,
                'Amount_services': self.price_service,
                'Pay': pay,
                'People': self.info['People'],
                'Child_8': self.info['Child_8'],
                'Child_16': self.info['Child_16'],
                'Leader': self.info['Leader'],
                'Summary': self.summary,
                'Services': listing,
                'id': self.id
                }
        return data

    # Подготовить чек
    def load_check(self):
        self.payment_check.prepare_check(self)
        return

    # Распечатка чека
    def print_check(self):
        return self.payment_check.print_check()

    # Отправить группу
    def request_group(self, pay):
        data = self.load_data(pay)
        return self.request.write_group(data, pay)


class StoreGroup:
    def __init__(self, data):
        self.id = 0
        self.date = datetime.date.today()
        self.change_service = []
        self.price_service = 0
        self.summary = 0
        self.info = {}
        self.group = data
        self.record()

    # Сохранить состояние группы
    def record(self):
        self.date = self.group['ticket']['Created']
        self.change_service = self.group['list_service']
        self.price_service = self.group['ticket']['Amount_services']
        self.summary = self.group['ticket']['Summary']
        self.id = self.group['ticket']['id']
        for key, val in self.group.info.items():
            self.info[key] = val
        return

    # Переписать информацию от одного объекта другому
    @staticmethod
    def copy_group(donor, recipient):
        recipient.date = donor.date
        recipient.change_service = donor.change_service
        recipient.price_service = donor.price_service
        recipient.summary = donor.summary
        recipient.id = donor.id
        recipient.info = donor.info
        return

    # Записать группу на хранение
    def set_group(self, group):
        self.copy_group(group, self)
        return

    # Создать объект группы
    def get_group(self, parent):
        group = Group(parent)
        self.copy_group(self, group)
        return group
