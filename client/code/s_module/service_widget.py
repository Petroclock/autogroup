# coding=utf-8
from kivymd.label import MDLabel
from kivy.metrics import dp
from kivymd.list import OneLineAvatarIconListItem
from kivymd.list import IRightBodyTouch
from kivymd.selectioncontrols import MDCheckbox


class TitleLabel(MDLabel):
    def __init__(self, text, **kwargs):
        super(TitleLabel, self).__init__(**kwargs)
        self.theme_text_color = 'Secondary'
        self.font_style = "Title"
        self.size_hint_y = None
        self.height = dp(36)
        self.text = text


class ContentLabel(MDLabel):
    def __init__(self, text, **kwargs):
        super(ContentLabel, self).__init__(**kwargs)
        self.theme_text_color = 'Primary'
        self.text = text


class ServiceItem(OneLineAvatarIconListItem):
    def __init__(self, group, service, **kwargs):
        super(ServiceItem, self).__init__(**kwargs)
        self.group = group
        self.service = service
        self.text = u'Услуга: ' + self.service['name'] + u' Цена: ' + str(self.service['price'])
        self.check_box = IconLeftSampleWidget()
        self.check_box.fbind('on_press', self.check_service)
        self.add_widget(self.check_box)

    def data_service(self):
        return {'price': self.service['price'],
                'name': self.service['name']}

    def check_service(self, checkbox):
        if checkbox.active and not self.is_change():
            print('Add Service')
            self.group.change_service.append(self.data_service())
        elif not checkbox.active and self.is_change():
            print('Delete Service')
            self.group.change_service.remove(self.data_service())

    def is_change(self):
        for item in self.group.change_service:
            if item['name'] == self.service['name']:
                return True
        return False


class IconLeftSampleWidget(IRightBodyTouch, MDCheckbox):
    pass
