# coding=utf-8
from widgets import NumText, StrText, AllTextField
from kivy.uix.gridlayout import GridLayout


# Класс Информации
class Info(GridLayout):
    def __init__(self, group, **kwargs):
        super(Info, self).__init__(**kwargs)

        self.cols = 1
        self.padding = [20, 20]
        self.spacing = 5, 5
        self.text_field = group.fields.info_text_field
        self.num_field = group.fields.info_num_field
        self.str_field = group.fields.info_str_field
        self.group = group

    def build(self):
        group = self.group
        for key, val in group.fields.info_fields.items():
            if key in self.text_field:
                widget = AllTextField(text=val.decode('utf8'), name=key, id=key)
            elif key in self.num_field:
                widget = NumText(text=val.decode('utf8'), name=key, id=key)
            elif key in self.str_field:
                widget = StrText(text=val.decode('utf8'), name=key, id=key)
            if key == 'Created':
                widget.callback_focus(group.date_picker)
            widget.callback_text(group.get_data)
            print widget
            self.add_widget(widget)
