# -*- coding: utf-8 -*-
import datetime
import Func
from s_module.printer import Printer
from s_module.request import Request
from s_module.modal import Statistics as Stats
from kivymd.textfields import MDTextField
from kivy.uix.textinput import TextInput
from collections import OrderedDict


class ReformatCheck:
    def __init__(self, app):
        self.max_string_length = 28
        self.app = app
        # Строка разделяющая секции
        self.divider_section = u'=' * self.max_string_length
        # Данные чека форматирования
        self.title = []
        self.content = []
        self.summary = []
        self.barcode = 0
        # Данные группы
        self.benefits = OrderedDict({'l_25': u'-25% кол. чел. ',
                                     'l_50': u'-50% кол. чел. ',
                                     'l_100': u'-100% кол. чел. '})
        self.rows = {'people': u'Посетителей ',
                     'price': u'Стоимость ',
                     'translate': u'Услуги переводчика (+70%) ',
                     'place': u'Бурзянский район (-50%) ',
                     'total': u'Итог '}
        self.group_info = {'people': 0,
                           'l_25': 0,
                           'l_50': 0,
                           'l_100': 0,
                           'total': 0}
        self.services = []
        # Данные статистики
        self.count_tickets = 0
        self.total_sum = 0
        self.total_people = 0
        self.separator = u'  '
        self.filler = u' '
        self.len_number = 8
        self.len_time = 8
        self.len_total = 6
        # Данные чека
        self.count_string = 0
        self.length_string = 0
        self.printer = Printer(self)

    # ----------------------- Блок чека оплаты -----------------------

    # Обработка чека оплаты
    def payment(self, group):
        self.parse_group(group)
        self.prepare_title()
        self.prepare_payment(group)
        self.split_text(self.summary, self.rows['total'] + str(self.group_info['total']))
        self.final()

    # Разобрать объект группы
    def parse_group(self, group):
        info = {'Kol_vo': '', 'Lgoti_25': '', 'Lgoti_50': '', 'Lgoti_100': '', 'Lgita': '',
                'Yers8': '', 'Yers16': ''}
        for key, val in info.items():
            if isinstance(group.info[key], MDTextField) or isinstance(group.info[key], TextInput):
                info[key] = group.info[key].text
            elif isinstance(group.info[key], int) or isinstance(group.info[key], str):
                info[key] = group.info[key]

        self.group_info['people'] = info['Kol_vo']
        self.group_info['l_25'] = info['Lgoti_25']
        self.group_info['l_50'] = info['Lgoti_50'] + info['Lgita'] + info['Yers16']
        self.group_info['l_100'] = info['Lgoti_100'] + info['Yers8']
        self.group_info['total'] = int(group.itog_summa)
        self.services = group.change_service
        self.barcode = group.barcode
        return

    # Подготовить тело чека
    def prepare_payment(self, group):
        for service in self.services:
            if service['name'] == self.group_info['translator']:
                self.group_info['translate'] = True
            text = ''
            text += service['name'] + ' ' + str(service['price'])
            self.split_text(self.content, text)
            self.split_text(self.content, self.rows['people'] + str(self.group_info['people']))
            for key, val in self.benefits.items():
                if self.group_info[key] > 0:
                    self.split_text(self.content, val + str(self.group_info[key]))
            price_service = group.count_price(int(service['price']))
            self.split_text(self.content, self.rows['price'] + str(price_service))
            self.content.append(u'')
        return

    # ----------------------- Блок чека статистики -----------------------

    # Обработка чека статистики
    def statistic(self, content):
        self.prepare_title()
        self.prepare_statistic(content)
        result = self.prepare_total_stats()
        self.final()
        return result

    # Подготовка тела статистики
    def prepare_statistic(self, content):
        self.count_tickets = 0
        self.total_sum = 0
        self.total_people = 0
        self.format_row(u'Билет №', u'Время', u'Сумма')
        for i in content:
            numb = str(i['Number_ticket'])
            time = str(i['Time'])
            total = str(i['Summary'])
            self.format_row(numb, time, total)
            self.total_people = self.total_people + Func.val_to_int(i['People'])
            self.total_sum = self.total_sum + Func.val_to_int(i['Summary'])
            self.count_tickets += 1
        return

    # Подготовить строку
    def format_row(self, number, time, total):
        number = self.format_cols(number, self.len_number, 'left')
        time = self.format_cols(time, self.len_time, 'center')
        total = self.format_cols(total, self.len_total, 'right')
        text = number + self.separator + time + self.separator + total
        self.content.append(text)
        return

    def format_cols(self, col, max_len, direct):
        if len(col) < max_len:
            if direct == 'left':
                col.rjust(max_len, self.filler)
            elif direct == 'center':
                col.center(max_len, self.filler)
            elif direct == 'right':
                col.ljust(max_len, self.filler)
        elif len(col) > max_len:
            col = col[:max_len - 3] + '...'
        return col

    # Подготовка итога статистики
    def prepare_total_stats(self):
        self.summary.append(u'Итог: ')
        self.summary.append(u'-Билетов: ' + str(self.count_tickets))
        self.summary.append(u'-Сумма: ' + str(self.total_sum))
        text = u'Итого: \n' + u'-Билетов: ' + str(self.count_tickets) + u'\n-Сумма: ' + str(self.total_sum)
        return text

    # Распечатка
    def print_check(self):
        self.printer.print_check()
        return

    # ----------------------- Вспомогательный блок -----------------------

    # Подготовить заголовок чека
    def prepare_title(self):
        main = self.app
        title = [main.Titles['title1'], main.Titles['title1'], main.Titles['title1'], str(datetime.datetime.now())]
        self.reformat_title(title)

    def reformat_title(self, title):
        for item in title:
            self.title.append(item.center(self.max_string_length))
        return

    # Обрезать строку если она длинее чем максимум
    def split_text(self, content, text):
        if len(text) > self.max_string_length:
            l = len(text) - self.max_string_length
            cut = text[:len(text) - l].rfind(' ')
            content.append(text[0:cut])
            content.append(text[cut:])
        else:
            content.append(text)

    # Форматирование секции
    def format_string(self):
        sections = [self.content, self.summary]
        index = 0
        for section in sections:
            while index < len(section):
                section[index] = self.format_lines(section[index])
                index += 1

    # Форматирование цен
    def format_lines(self, row):
        if len(row) < self.max_string_length and len(row) != 0:
            space = row.rfind(u' ')
            half = row[0:space]
            half = half.ljust(self.max_string_length - len(row[space + 1:]), '_')
            row = half + row[space + 1:]
        elif len(row) == 0:
            row = self.divider_section
        return row

    # Количество строк в чеке
    def count_check(self):
        return len(self.title) + len(self.content) + len(self.summary)

    # Самая длинная строка в чеке
    def length_check(self):
        length = 0
        for item in self.content:
            if length < len(item):
                length = len(item)
        return length

    # Разделение на секции
    def section(self):
        sections = [self.title, self.content, self.summary]
        for item in sections:
            item.append(self.divider_section)

    def final(self):
        # Форматирует секции (построчно)
        self.format_string()
        # Разделяет секции
        self.section()

        # Характеристики чека
        self.count_string = self.count_check()
        self.length_string = self.length_check()
        return


class Payment:
    def __init__(self, app):
        self.app = app
        self.check = ReformatCheck(app)

    def reload(self):
        self.__init__(self.app)

    def prepare_check(self, group):
        self.check.payment(group)
        return

    # Распечатать чек
    def print_check(self):
        return self.check.print_check()


class Statistics:
    def __init__(self, app):
        self.app = app
        self.request = Request()
        self.request.link = self.app.link
        self.check = ReformatCheck(app)

    # Получить данные с сервера
    def get_data(self):
        return self.request.list_of_group()

    # Подготовить чек для распечатки
    def prepare_check(self, request):
        result = self.check.statistic(request)
        return result

    # Открыть модаль статистики
    def open_modal(self):
        request = self.get_data()
        if not request:
            return
        request = request['req']
        text = self.prepare_check(request)
        Stats(text, self)
        return

    # Распечатать статистику
    def print_stats(self):
        return self.check.print_check()

if __name__ == '__main__':
    print 'Check import'
