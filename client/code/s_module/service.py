# coding=utf-8
from kivy.app import App
from kivymd.list import MDList
from request import Request
from service_widget import ServiceItem
from kivy.uix.scrollview import ScrollView
from kivy.uix.gridlayout import GridLayout


# Класс Услуг
class Service(GridLayout):
    def __init__(self, group, **kwargs):
        super(Service, self).__init__(**kwargs)
        self.cols = 1
        self.padding = [10, 0, 40, 0]
        scroll = ScrollView(do_scroll_x=False)
        self.list_of_service = MDList()
        scroll.add_widget(self.list_of_service)
        self.add_widget(scroll)
        # Родительский виджет (Регистрация или Поиск группы)
        self.changed_services = group.change_service
        self.group = group
        self.request = Request()
        self.request.link = App.get_running_app().link

    # Выводится список услуг
    def create_services(self, list_widget, date):
        list_widget.clear_widgets()
        services = self.request.get_services(date)
        if not services:
            print 'No services: bad connect'
            return
        for item in services['service']:
            if not item['price']:
                continue
            obj = ServiceItem(self.group, item)
            list_widget.add_widget(obj)
        return
