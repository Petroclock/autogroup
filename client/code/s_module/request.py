# -*- coding: utf-8 -*-

import urllib
import json
from s_module.modal import Msg


class Request:
    def __init__(self):
        self.link = ''
        self.IOError = 'Ошибка подключения. Проверьте подключение и попробуйте снова.'
        self.ValueError = 'Ошибка значения. Технические неполадки на сервере.\nСообщите администратору.'
        self.app = 'AutoGroup/'
        self.controller = 'app/'
        self.string_vars = {
            'get_services': self.app + self.controller + 'services.json',
            'write_group': self.app + self.controller + 'new_group.json',
            'search_from_leader': self.app + self.controller + 'search_leader.json',
            'search_from_id': self.app + self.controller + 'search_id.json',
            'pay_bron': self.app + self.controller + 'payment.json',
            'list_of_group': self.app + self.controller + 'list_tickets.json',
            'connect_to_server': self.app + self.controller + 'check'
        }

    def url(self, name_func, data=None):
        try:
            return urllib.urlopen(self.link + self.string_vars[name_func], data)
        except IOError:
            Msg(self.IOError)
            return False

    def json_load(self, obj, exc_val):
        try:
            response = json.load(obj)
        except ValueError:
            response = exc_val
            Msg(self.ValueError)
        return response

    def get_services(self, date):
        if not self.connect_to_server:
            return False
        response = urllib.urlencode({'Date': date})
        uslugi = self.url('get_services', data=response)
        if uslugi:
            return self.json_load(uslugi, False)

    def write_group(self, data, pay):
        if not self.connect_to_server:
            return False
        request = urllib.urlencode(data)
        response = self.url('write_group', data=request)
        if not pay:
            response = self.json_load(response, False)
            if response:
                print response
                return str(response['id'])
        else:
            return True

    def search_from_leader(self, leader):
        if not self.connect_to_server:
            return False
        try:
            leader = str(leader)
        except UnicodeEncodeError:
            leader = leader.encode('utf8')
        req = urllib.urlencode({'Leader': leader})
        response = self.url('search_from_leader', data=req)
        if response is not False:
            return self.validate_search(response)

    def search_from_id(self, group_id):
        if not self.connect_to_server:
            return False
        req = urllib.urlencode({'id': group_id})
        response = self.url('search_from_id', data=req)
        if response is not False:
            return self.validate_search(response)

    def validate_search(self, response):
        response = self.json_load(response, False)
        if not response:
            return False
        if (len(response['Data'])) == 0:
            Msg('Запись не найдена. Проверьте данные, и попробуйте ещё раз.')
            return False
        elif (len(response['Data'])) >= 1:
            content = 'Найдено записей: ' + str(len(response['Data']))
            Msg(content)
        return response

    def pay_bron(self, group_id, point_number):
        if not self.connect_to_server:
            return False
        req = urllib.urlencode({'id': group_id, 'Point': point_number})
        if self.url('pay_bron', data=req) is not False:
            return True

    # Функция проверки соединения
    def connect_to_server(self):
        if len(self.link) == 0:
            return False
        if self.url('connect_to_server') is not False:
            return True

if __name__ == '__main__':
    print 'Request import'
