# -*- coding: utf-8 -*-

from kivy.metrics import dp
from kivymd.dialog import MDDialog
from kivymd.label import MDLabel


class Msg:
    def __init__(self, text):
        content = MDLabel(font_style='Body1',
                          theme_text_color='Secondary',
                          text=text,
                          valign='top')
        content.bind(size=content.setter('text_size'))
        self.dialog = MDDialog(title="Сообщение",
                               content=content,
                               size_hint=(.4, None),
                               height=dp(200),
                               auto_dismiss=False)
        self.dialog.add_action_button("Закрыть", action=lambda *x: self.dialog.dismiss())
        self.dialog.open()


class Statistics:
    def __init__(self, text, main):
        self.text = text
        self.main = main
        content = MDLabel(font_style='Body1',
                          theme_text_color='Secondary',
                          text=self.text,
                          valign='top')
        content.bind(size=content.setter('text_size'))
        self.dialog = MDDialog(title="Статистика",
                               content=content,
                               size_hint=(.5, None),
                               height=dp(300),
                               auto_dismiss=False)
        self.dialog.add_action_button("Закрыть", action=lambda *x: self.dialog.dismiss())
        self.dialog.add_action_button("Распечатать", action=lambda *x: self.print_statistic())
        self.dialog.open()

    def print_statistic(self):
        self.main.print_stats()
        self.dialog.dismiss()
