# -*- coding: utf-8 -*-

import cups


class Printer:

    def __init__(self, check_dict):
        self.check = 'check.txt'
        self.error_msg = 'Ошибка распечатки.\nПроверьте ваш принтер \nи попробуйте ещё раз'
        self.check_dict = check_dict

    # Распечатать чек оплаты
    def print_check(self):
        self.create_check()
        return self.print_file(self.check)

    # Печать файла
    def print_file(self, document):
        conn = cups.Connection()
        printers = conn.getPrinters()
        if len(printers) > 0:
            printer_name = printers.keys()[0]
            return conn.printFile(printer_name, document, "Python_Status_print", {})

    # Заполнение чека
    def create_check(self):
        f = open(self.check, 'w')
        sections = [self.check_dict.title, self.check_dict.content, self.check_dict.summary]
        for section in sections:
            for item in section:
                f.write(item.encode('utf8') + '\n')
        f.close()

if __name__ == '__main__':
    print 'Printer import'
