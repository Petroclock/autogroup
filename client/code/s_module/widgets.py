# coding=utf-8
import re
from kivy.metrics import dp
from kivymd.label import MDLabel
from kivymd.button import MDRaisedButton, MDFlatButton
from kivymd.textfields import MDTextField
from kivymd.card import MDCard
from kivymd.card import MDSeparator
from kivy.uix.boxlayout import BoxLayout


class Label(MDLabel):
    def __init__(self, text, **kwargs):
        super(Label, self).__init__(**kwargs)
        self.id = 'label'
        self.font_style = 'Title'
        self.theme_text_color = 'Primary'
        self.halign = 'center'
        self.text = text


class Btn(MDRaisedButton):
    def __init__(self, text, **kwargs):
        super(Btn, self).__init__(**kwargs)
        self.text = text
        self.elevation_normal = 2
        self.opposite_colors = True


class FlatBtn(MDFlatButton):
    def __init__(self, text, **kwargs):
        super(FlatBtn, self).__init__(**kwargs)
        self.text = text
        # self.elevation_normal = 2
        # self.opposite_colors = True


class InfoTextField(MDTextField):
    def __init__(self, text, info, **kwargs):
        super(InfoTextField, self).__init__(**kwargs)
        self.hint_text = text
        self.message = info
        self.message_mode = "persistent"


class AllTextField(MDTextField):
    def __init__(self, text, name, id, **kwargs):
        super(AllTextField, self).__init__(**kwargs)
        self.hint_text = text
        self.name = name
        self.id = id

    def callback_text(self, callback):
        self.bind(text=callback)

    def callback_focus(self, callback):
        self.bind(focus=callback)


# Буквенный текст
class StrText(MDTextField):
    str = re.compile('\D')

    def __init__(self, text, name, id, **kwargs):
        super(StrText, self).__init__(**kwargs)
        self.hint_text = text
        self.name = name
        self.id = id

    def callback_text(self, callback):
        self.bind(text=callback)

    def callback_focus(self, callback):
        self.bind(focus=callback)

    def insert_text(self, substring, from_undo=False):
        s = substring
        if self.str.search(s) is None:
            s = ''
        return super(StrText, self).insert_text(s, from_undo=from_undo)


# Числовой текст
class NumText(MDTextField):
    num = re.compile('\d')

    def __init__(self, text, name, id, **kwargs):
        super(NumText, self).__init__(**kwargs)
        self.hint_text = text
        self.name = name
        self.id = id

    def callback_text(self, callback):
        self.bind(text=callback)

    def callback_focus(self, callback):
        self.bind(focus=callback)

    def insert_text(self, substring, from_undo=False):
        s = substring
        if self.num.search(s) is None:
            s = ''
        return super(NumText, self).insert_text(s, from_undo=from_undo)


class GroupCard(MDCard):
    def __init__(self, group=None, change=None, pay=None, **kwargs):
        super(GroupCard, self).__init__(**kwargs)
        self.size_hint = (1, None)
        self.height = dp(180)

        self.group = group
        self.change = change
        self.pay = pay

        content = BoxLayout(orientation='vertical')
        content.add_widget(self.add_content())
        content.add_widget(MDSeparator(height=dp(1)))
        content.add_widget(self.add_button())
        self.add_widget(content)

    def add_content(self):
        if self.group is None:
            self.fill_group()
        group = self.group
        layout = BoxLayout(orientation='vertical',
                           padding=dp(8))
        layout.add_widget(MDLabel(text='№ ' + str(group['Number']),
                                  theme_text_color='Secondary',
                                  font_style="Title",
                                  size_hint_y=None,
                                  height=dp(36)))
        layout.add_widget(MDSeparator(height=dp(1)))
        layout.add_widget(MDLabel(text='Руководитель: ' + group['Leader'],
                                  theme_text_color='Primary'))
        layout.add_widget(MDLabel(text='Откуда: ' + group['Location'],
                                  theme_text_color='Primary'))
        layout.add_widget(MDLabel(text='Посетителей:' + str(group['People']) + ' человек',
                                  theme_text_color='Primary'))
        return layout

    def fill_group(self):
        if self.group is None:
            self.group = {'Number': '',
                          'Leader': '',
                          'Location': '',
                          'People': ''}
        return self.group

    def add_button(self):
        layout = BoxLayout()
        change_btn = Btn(text='Изменить')
        if self.change is not None:
            change_btn.fbind('on_press', self.change)
        pay_btn = Btn(text='Оплатить')
        if self.pay is not None:
            pay_btn.fbind('on_press', self.pay)
        layout.add_widget(change_btn)
        layout.add_widget(pay_btn)
        return layout
